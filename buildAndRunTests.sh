cd ~/Sources/my/rise2/
~/Sources/my/rise2/buildAndRunTests.sh

cd ~/Sources/my/flamesteelengine2
rm -rf build
mkdir build
cd build
python ~/Sources/my/rise2beckett/rise2beckett.py ../src
python ~/Sources/my/rise2/rise2.py ../src/main.ri2 main.cpp "<NONE>"
mv rise2-translated.cpp main.cpp
python ~/Sources/my/rise2/rise2.py ../src/Controller.ri2 Controller.hpp "FlameSteelEngine2"
mv rise2-translated.cpp Controller.cpp 
clang++ -std=c++20 main.cpp -o flamesteelengine2app
chmod +x flamesteelengine2app
./flamesteelengine2app